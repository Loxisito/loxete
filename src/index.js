require("dotenv").config()
const { prefix, avatar } = require('./config.json')

const Discord = require('discord.js')
const client = new Discord.Client()

//Set commands
const fs = require('fs')
client.commands = new Discord.Collection()
const commandFolders = fs.readdirSync('./commands')
for (const folder of commandFolders) {
	const commandFiles = fs.readdirSync(`./commands/${folder}`).filter(file => file.endsWith('.js'))
	for (const file of commandFiles) {
		const command = require(`./commands/${folder}/${file}`)
		client.commands.set(command.name, command)
	}
}

const commandHandler = require('./commandHandler')
const pingme = require('./pingme')


//Set bots status while ready
client.on("ready", () =>{
    console.log(`Logged in as ${client.user.tag}!`)
    client.user.setPresence({
        status: "dnd", // online, idle, dnd, invisible
        activity: {
            name: prefix + "help 🥰",
            type: "LISTENING" //PLAYING, WATCHING, LISTENING, STREAMING
        }
    })
 })


client.on('message', (msg) => {
	if (msg.author.bot) return

	//Ping me on user mention
	if (/l+o+(x+|k+s+z*)((i|í)*|(i|í)+s+(i|í)*t+(o|ó)+)[^a-zA-Z1-9]/i.test(msg))
		pingme.execute(client, msg)

	//Command handler
    if (!msg.content.startsWith(prefix)) return

	const args = msg.content.slice(prefix.length).trim().split(/ +/)
    commandHandler.execute(msg, args)

})
client.login(process.env.TOKEN)
