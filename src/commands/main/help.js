const { prefix, name, avatar } = require('../../config.json')
const Discord = require('discord.js')

module.exports = {
	name: 'help',
	description: 'Prints help. Max. 3 commands at once. List of all the commands below',
	aliases: ['h','commands','cmds'],
	usage: `${prefix}help [command(s)]`,
	args: false,
	cooldown: 5,


	async execute(msg, args) {

		if(!args.length) {
    		const embed = new Discord.MessageEmbed()
				.setColor('#B97839')
				.setTitle(`${prefix}help`)
				.setDescription(`aliases: \`${module.exports.aliases.join('\`, \`')}\``)
				.setFooter(name, avatar)
				.addFields(
					{ name: 'Usage', value: `${module.exports.usage}`, inline: true },
					{ name: 'Description', value: `${module.exports.description}`, inline: false },
				)
			commands = []
			const fs = require('fs')
			const commandFolders = fs.readdirSync('./commands')
			for (const folder of commandFolders) {
				const commandFiles = fs.readdirSync(`./commands/${folder}`).filter(file => file.endsWith('.js'))
				for (const file of commandFiles) {
					commands.push(file.slice(0,-3))
				}
				embed.addField(folder, `\`${commands.join('\`, \`')}\``, true)
				commands = []
			}
			try {
				msg.channel.send(embed)
			} catch {
				throw new Error('Couldn\'t send embed')
			}
		} else {
			const { commands } = msg.client

			// list prevents from printing the same command more than once
			var list = []

			args.forEach(element => {
				var name = element.toLowerCase()
				var command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

				if (!command || !list.indexOf(command) || list.length==3) return
				list.push(command)

				const embed = new Discord.MessageEmbed()
					.setColor('#B97839')
					.setFooter(name, avatar)
    				.addFields()
					.setTitle(`${prefix}${command.name}`)
					.setDescription(`aliases: \`${command.aliases.join('\`, \`')}\``)
					.addField('Usage:', `${command.usage}`)
					.addField('Description:', `${command.description}`)
				try {
					msg.channel.send(embed)
				} catch {
					throw new Error('Couldn\'t send embed')
				}
			})
		}
	}
}
