const { prefix } = require('../../config.json')

module.exports = {
	name: "ping",
	aliases: ["pong"],
	description: "Mira si el bot está",
	usage: `${prefix}ping`,
	args: false,
	cooldown: 5,


	async execute(msg, args) {
		msg.channel.send(`Ay, no mames xd`).then(m => {
			setTimeout(() => {
				const ping = m.createdTimestamp - msg.createdTimestamp
				m.delete()
				m.channel.send(`ping: ${ping}ms`)
            }, 2000)
		})
	}
}
