const { prefix } = require('../../config.json')
require('discord.js')

module.exports = {
	name: "purge",
	aliases: ["prune", "delete", "del"],
	description: "Borra los últimos 100 mensajes del canal.",
	usage: `${prefix}purge [num]`,
	args: false,


	async execute(msg,args) {
        await msg.delete()
        if(!args.length) {
        	msg.channel.bulkDelete(await msg.channel.messages.fetch({limit: 99}))
        } else if(isNaN(args[0])) {
        	msg.reply(`\`${args[0]}\` is not a number`)
        		.then(m => {
        			setTimeout(() => m.delete(), 5000)
        		})
        } else if(args[0]<1) {
        	msg.reply(`The number of messages has to be at least \`1\`; You tried to delete \`${args[0]}\` messages.`)
        		.then(m => {
        			setTimeout(() => m.delete(), 5000)
        		})
        } else {
        	const lim = parseInt(args[0],10)
        	msg.channel.bulkDelete(await msg.channel.messages.fetch({limit: lim}))
        }

	},

}
