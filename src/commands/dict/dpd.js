const { prefix } = require('../../config.json');

module.exports = {
	name: "dpd",
	aliases: ["dudas", "duda"],
	description: "Envía enlaces de artículos del Diccionario Panhispánico de Dudas [DPD]",
	usage: `${prefix}dpd <título>`,
	args: false,


	async execute(msg,args) {
		const search = args.join(" ")
		msg.channel.send(`<https://rae.es/dpd/${encodeURI(search)}>`)
	},
	
}
