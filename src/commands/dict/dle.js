const { prefix } = require('../../config.json');

module.exports = {
	name: "dle",
	aliases: ["rae", "dic", "dict"],
	description: "Envía enlaces de definiciones de las palabras del Diccionario de la Lengua Española [DLE]",
	usage: `${prefix}dle <título>`,
	args: false,


	async execute(msg, args) { 
		const search = args.join(" ")
		msg.channel.send(`<https://dle.rae.es/${encodeURI(search)}>`)
	},
}
