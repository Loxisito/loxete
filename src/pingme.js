const { pingsChan, loxId } = require('./config.json')

module.exports = {
	name: "pingme",
	async execute(client, msg) { 
		const chan = client.channels.cache.get(pingsChan)
		await chan.send(`**By ${msg.author.username} \`${msg.author.id}\` in ${msg.guild.name}** (#${msg.channel.name}):\n> ${msg.content}\n${msg.url}\n<@${loxId}>`)
	}
}
