module.exports = {
	async execute(msg,args) {
		const { client } = msg
		//Handle commands with ./commands files
		const fs = require('fs')
		const commandFolders = fs.readdirSync('./commands');

		for (const folder of commandFolders) {
			const commandFiles = fs.readdirSync(`./commands/${folder}`).filter(file => file.endsWith('.js'));
			for (const file of commandFiles) {
				const command = require(`./commands/${folder}/${file}`);
				client.commands.set(command.name, command);
			}
		}


		const commandName = args.shift().toLowerCase()
		const command = client.commands.get(commandName)
			|| client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName))

		if (!command) return
		if (command.args && !args.length) {
			const help = client.commands.get('help')
			return msg.reply(`**This command requires arguments**
Usage:    \`${command.usage}\`
⚠️ Arguments are either \`<required>\` or \`[optional]\`. Don't write the \`<>\`/\`[]\` symbols.
Use \`${help.usage}\` if you need more information`)
		}

		try {
				command.execute(msg, args)
		} catch (error) {
			console.error(error)
			msg.reply('⚠️ Ha habido un error mientras intentábamos ejecutar el comando.')
		}
	}
}
